#include <assert.h>
#include "list.h"




//  node functions---------------------------

/* Create new list node
 * returns NULL on failure */
ListNode *list_node_new(void *val) {

    ListNode *self;

    if(!(self = malloc(sizeof(ListNode)))) {

        return NULL;
    }

    self->next = NULL;
    self->prev = NULL;
    self->val = val;

    return self;
}

/* Get value of this list node
 * returns NULL on failure */
void *list_node_getval(ListNode *node) {

    if(node != NULL) {
        return node->val;
    } else {
        return NULL;
    }
}

void list_node_destroy(ListNode *self) {

    free(self);
}




//  list functions---------------------------

/* Create new list
 * returns NULL on failure */
List *list_new() {

    List *self;

    if(!(self = malloc(sizeof(List)))) {

        return NULL;
    }

    self->head = NULL;
    self->tail = NULL;
    self->len = 0;

    return self;
}

/* Destroys 1) all elements -> free(node)
 *          2) list -> free(list) */
void list_destroy(List *self) {

    assert(self != NULL);

    unsigned int len = self->len;
    ListNode *next;
    ListNode *cur = self->head;

    while(len--) {

        next = cur->next;

        free(cur);
        cur = next;
    }

    free(self);
}

unsigned int list_size(List *self) {

    assert(!(self == NULL));

    return self->len;
}

/* Append the node to the list (Append - add from the end)
 * and return it
 * returns NULL on failure */
ListNode *list_push_to_tail(List *self, ListNode *node) {

    if(!self) {
        return NULL;
    }

    if(!node) {
        return NULL;
    }

    if(self->len) {

        node->prev = self->tail;
        node->next = NULL;

        self->tail->next = node;
        self->tail = node;
    } else {

        self->head = self->tail = node;
        node->prev = node->next = NULL;
    }

    ++self->len;
    return node;
}

/* Prepend the node to the list (Prepend - add from the start)
 * and return it
 * returns NULL on failure */
ListNode *list_push_to_head(List *self, ListNode *node) {

    if(!self) {
        return NULL;
    }

    if(!node) {
        return NULL;
    }

    if(self->len) {

        node->next = self->head;
        node->prev = NULL;

        self->head->prev = node;
        self->head = node;
    } else {

        self->head = self->tail = node;
        node->prev = node->next = NULL;
    }

    ++self->len;
    return node;
}

/* Remove node by "value + pointer" from the list */
void list_remove(List *self, ListNode *node) {

    assert(self != NULL);

    if(node == NULL) {
        return;
    }

    if (node->prev) {
        node->prev->next = node->next;
    } else {
        self->head = node->next;
    }

    if (node->next) {
        node->next->prev = node->prev;
    } else {
        self->tail = node->prev;
    }

    free(node);
    --self->len;
}

/* Detach last node from the list
 * and return it
 * returns NULL on failure */
ListNode *list_pop_from_tail(List *self) {

    if(!self->len) {
        return NULL;
    }

    ListNode *node = self->tail;

    if(--self->len) {

        (self->tail = node->prev)->next =NULL;
    } else {

        self->tail = self->head = NULL;
    }

    node->next = node->prev = NULL;
    return node;
}

/* Detach first node from the list
 * and return it
 * returns NULL on failure */
ListNode *list_pop_from_head(List *self) {

    if(!self->len) {
        return NULL;
    }

    ListNode *node = self->head;

    if(--self->len) {

        (self->head = node->next)->prev = NULL;
    } else {

        self->head = self->tail = NULL;
    }

    node->next = node->prev = NULL;
    return node;
}

/* Return the node at the given index
 * if index < 0 => index from the tail
 * else => from the head
 * returns NULL on failure */
ListNode *list_at(List *self, int index) {

    if(!self->len) {
        return NULL;
    }


    ListDirection direction;

    if(index < 0) {
        direction = LIST_TAIL;
        index = ~index;
    } else if(index >= 0){
        direction = LIST_HEAD;
    } else {
        return NULL;
    }

    if((unsigned)index < self->len) {

        ListIterator *iter = list_iterator_new(self, direction);

        if(!iter) {

            return NULL;
        }

        ListNode *node = list_iterator_next(iter);

        while(index--) {

            node = list_iterator_next(iter);
        }

        list_iterator_destroy(iter);
        return node;
    }

    return NULL;
}




//  list iterator functions------------

/* Creates Iterator from head/tail
 * returns NULL on failure */
ListIterator *list_iterator_new(List *list, ListDirection direction) {

    ListNode *node;

    if (direction == LIST_HEAD) {
        node = list->head;
    } else if(direction == LIST_TAIL) {
        node = list->tail;
    } else {
        return NULL;
    }

    return list_iterator_new_from_node(node, direction);
}

/* Creates Iterator from node
 *  returns NULL on failure */
ListIterator *list_iterator_new_from_node(ListNode *node, ListDirection direction) {

    ListIterator *self;

    if(node == NULL) {
        return NULL;
    }

    if(!(self = malloc(sizeof(ListIterator)))) {
        return NULL;
    }

    self->next = node;
    self->direction = direction;

    return self;
}

/*
 * If the list is empty, returns NULL */
ListNode *list_iterator_next(ListIterator *self) {

    if(self == NULL) {
        return NULL;
    }

    ListNode *cur = self->next;

    if(cur) {

        if (self->direction == LIST_HEAD) {
            self->next = cur->next;
        } else {
            self->next = cur->prev;
        }
    }

    return cur;
}

/* Deletes this iterator */
void list_iterator_destroy(ListIterator *self) {

    free(self);
    self = NULL;
}