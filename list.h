//"interface" list -- list.h

#ifndef LIST_LIST_H

#define LIST_LIST_H

#include <stdlib.h>

//------------------- declarations --------------------------
typedef enum {
    LIST_HEAD,
    LIST_TAIL
} ListDirection;

typedef struct ListNode ListNode;
typedef struct List List;
typedef struct ListIterator ListIterator;

struct List {
    ListNode *head; // root - first element
    ListNode *tail; // last element
    unsigned int len;
};

struct ListNode {
    ListNode *prev;
    ListNode *next;
    void *val;
};

struct ListIterator {
    ListNode *next;
    ListDirection direction;
};

//------------------- node functions------------------------

ListNode *list_node_new(void *val);

void list_node_destroy(ListNode *self);

void *list_node_getval(ListNode *node);

//------------------- list functions ------------------------
List *list_new();

ListNode *list_push_to_tail(List *self, ListNode *node);

ListNode *list_push_to_head(List *self, ListNode *node);

ListNode *list_at(List *self, int index);

ListNode *list_pop_from_tail(List *self);

ListNode *list_pop_from_head(List *self);

unsigned int list_size(List *self);

void list_remove(List *self, ListNode *node);

void list_destroy(List *self);

//------------------- iterator functions --------------------
ListIterator *list_iterator_new(List *list, ListDirection direction);

ListIterator *list_iterator_new_from_node(ListNode *node, ListDirection direction);

ListNode *list_iterator_next(ListIterator *self);

void list_iterator_destroy(ListIterator *self);

#endif // LIST.H