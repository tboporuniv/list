#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "list.h"

#define N 4
#define N_CUT 3
#define N_PRINT 10


void
init_values(int *values) {

    for (unsigned int i = 1; i <= 2*N; ++i)
        values[i] = i;
}

// Check allocation errors
void create_list(List **list) {

    printf("--------------------\n1) Creating List...\n");

    //Retry untill there are no allocating errors
    while(!(*list = list_new()));
    assert(list_size(*list) == 0);

    printf("Creating list - complete!\n--------------------\n");
}

void fill_list(List *list, int *values) {

    printf("--------------------\n2)\n Filling list...");

    unsigned int initial_size = list_size(list);

    ListNode* node;

    for (unsigned int i = 1; i <= N; ++i) {

        while(!(node = list_node_new(&values[i + N - 1])));
        list_push_to_tail(list, node);

        while(!(node = list_node_new(&values[N - i])));
        list_push_to_head(list, node);

        assert(list_size(list) == (initial_size + i*2));
    }

    printf("\nFilling test - complete!\n--------------------\n");
}

void print_list(List *list) {

    printf("--------------------\n3) Printing list...\n");

    ListIterator *iter = NULL;
    while(!(iter = list_iterator_new(list, LIST_TAIL)));

    ListNode *node = NULL;
    unsigned int i = 0;
    while ((node = list_iterator_next(iter))) {

        printf("%d ", *(int*)list_node_getval(node));
        ++i;
        if (i > N_PRINT) {

            printf("...");
            break;
        }
    }

    printf("\nPrinting list - complete!\n--------------------\n");

    list_iterator_destroy(iter);
}

ListNode *check_list_at(List* list, int index) {

    ListNode* temp = NULL;
    while(!(temp = list_at(list, index)));
    return temp;
}

void check_integrity(List *list) {

    printf("--------------------\n4)\n Check integrity...\n");

    ListIterator *iter = NULL;
    while (!(iter = list_iterator_new(list, LIST_TAIL)));

    ListNode *node = NULL, *temp = NULL;

    // iterating to end
    unsigned int i = 0;
    while ((temp = list_iterator_next(iter))) {

        node = temp;
        ++i;
    }

    // check that size equals
    assert(i == list_size(list));

    // check no more elements are there
    assert(list_iterator_next(iter) == NULL);

    // check last node is the one we've got to
    assert(node == list_pop_from_head(list));
    list_push_to_tail(list, node);

    assert(node == check_list_at(list, list_size(list) - 1));

    assert(node == check_list_at(list, -1));

    // repeat for reverse iterator
    list_iterator_destroy(iter);

    while(!(iter = list_iterator_new(list, LIST_HEAD)));

    // iterating to front
    i = 0;
    while ((temp = list_iterator_next(iter))) {

        node = temp;
        ++i;
    }

    // check that size equals
    assert(i == list_size(list));

    // check no more elements are there
    assert(list_iterator_next(iter) == NULL);

    // check last node is the one we've got to
    assert(node == list_pop_from_tail(list));
    list_push_to_head(list, node);
    assert(node == check_list_at(list, 0));

    list_iterator_destroy(iter);

    printf("Integrity test - complete!\n--------------------\n");
}

void cut_list(List *list) {

    printf("--------------------\n5)\n Cutting list down...");

    unsigned int initial_size = list_size(list);

    for (unsigned int i = 1; i <= N_CUT; ++i) {

        list_node_destroy(list_pop_from_tail(list));
        list_node_destroy(list_pop_from_head(list));

        assert(list_size(list) == (initial_size - (i*2)));
    }

    initial_size = list_size(list);
    list_remove(list, check_list_at(list, 0));
    assert(list_size(list) == (initial_size - 1));

    initial_size = list_size(list);
    list_remove(list, check_list_at(list, -1));
    assert(list_size(list) == (initial_size - 1));

    printf("\nCutting test - complete!\n--------------------\n");
}

void check_failures(List* list) {

    printf("--------------------\n6)\n Going through failure returns...");

    assert(list_push_to_tail(list, NULL) == NULL);
    assert(list_push_to_head(list, NULL) == NULL);
    assert(list_at(list, list_size(list)) == NULL);


    while(!(list = list_new()));

    assert(list_pop_from_tail(list) == NULL);
    assert(list_pop_from_head(list) == NULL);

    int a  = 237;

    ListNode *node = NULL;

    // Check inserts and pops and removes for last elements
    while(!(node = list_node_new(&a)));
    list_push_to_tail(list, node);
    node = list_pop_from_head(list);
    assert(*(int*)list_node_getval(node) == 237);
    list_node_destroy(node);

    while(!(node = list_node_new(&a)));
    list_push_to_head(list, node);
    node = list_pop_from_head(list);
    assert(*(int*)list_node_getval(node) == 237);
    list_node_destroy(node);

    while(!(node = list_node_new(&a)));
    list_push_to_tail(list, node);
    list_remove(list, check_list_at(list, 0));

    assert(list_size(list) == 0);
    assert(list_pop_from_tail(list) == NULL);

    list_destroy(list);

    printf("\nFailures test - complete\n--------------------\n");
}

int main() {

    int *values = calloc(2*N+1, sizeof(int));

    List *list = NULL;

    init_values(values);

    create_list(&list);
    fill_list(list, values);
    print_list(list);
    check_integrity(list);
    cut_list(list);
    check_failures(list);

//    ListNode *node = list_at(list, -10);
//    int val = *(int*)list_node_getval(node);
//    printf("ALO %d\n", val);

    list_destroy(list);
    free(values);

    printf("\n--------------------\nPassed all the tests!\n--------------------\n");
    return 0;
}